Ansible Docker SSH
==================

What this playbook does:

- installs docker-py onto Docker host if it isn't installed yet
- creates 2 Docker containers
- creates 'docker_root' user
- copies SSH private key into 1st container
- copies SSH public key into 2nd container
- installs sudo into 1st container
- installs SSH server into 2nd container and starts it
- checks SSH passwordless connectivity from 1st container to 2nd one.

